1.  [Back-end Development Handbook](index.html)
2.  [Back-end Development
    Handbook](Back-end-Development-Handbook_262349.html)
3.  [MEETING NOTE](MEETING-NOTE_25198629.html)
4.  [01. Weekly Review](01.-Weekly-Review_27066369.html)
5.  [Q2-2022](Q2-2022_26869761.html)

# Back-end Development Handbook : 2022-08-03.Department-Weekly-Review

Created by Thao Tran, last modified on Aug 03, 2022

|                  |                                                                                            |
|------------------|--------------------------------------------------------------------------------------------|
| **Date**         | 03-08-2022                                                                                 |
| **Time**         | 10h5-11h00                                                                                 |
| **Participants** | <a                                                                                         
                    href="https://gotecq-vn.atlassian.net/wiki/people/627e391219b12900682ac0fc?ref=confluence"  
                    class="confluence-userlink user-mention current-user-mention"                               
                    data-username="627e391219b12900682ac0fc"                                                    
                    data-account-id="627e391219b12900682ac0fc" target="_blank"                                  
                    data-linked-resource-id="655401" data-linked-resource-version="1"                           
                    data-linked-resource-type="userinfo"                                                        
                    data-base-url="https://gotecq-vn.atlassian.net/wiki">Thao Tran</a> <a                       
                    href="https://gotecq-vn.atlassian.net/wiki/people/6000fd1e64208901415ca478?ref=confluence"  
                    class="confluence-userlink user-mention"                                                    
                    data-username="6000fd1e64208901415ca478"                                                    
                    data-account-id="6000fd1e64208901415ca478" target="_blank"                                  
                    data-linked-resource-id="33036" data-linked-resource-version="1"                            
                    data-linked-resource-type="userinfo"                                                        
                    data-base-url="https://gotecq-vn.atlassian.net/wiki">Hung Le</a>                            |
| **Location**     | 4th floor                                                                                  |
| **Meeting type** | GG Meeting                                                                                 |

## <img src="images/icons/emoticons/72/1f4bc.png"
class="emoticon emoticon-blue-star" data-emoji-id="1f4bc"
data-emoji-shortname=":briefcase:" data-emoji-fallback="💼"
data-emoticon-name="blue-star" width="16" height="16"
alt="(blue star)" /> Objectives

- <a href="https://gotecq-vn.atlassian.net/browse/TPF-1756"
  class="jira-issue-key"><img
  src="https://gotecq-vn.atlassian.net/images/icons/issuetypes/epic.svg"
  class="icon" />TPF-1756</a> - CDM + PRC \> Contract Management v1.3.0
  Completed

- <a href="https://gotecq-vn.atlassian.net/browse/TPF-1921"
  class="jira-issue-key"><img
  src="https://gotecq-vn.atlassian.net/images/icons/issuetypes/epic.svg"
  class="icon" />TPF-1921</a> - DAS \> PCP Dashboard v1.1.0 Completed

- <a href="https://gotecq-vn.atlassian.net/browse/TPF-1450"
  class="jira-issue-key"><img
  src="https://gotecq-vn.atlassian.net/images/icons/issuetypes/epic.svg"
  class="icon" />TPF-1450</a> - EHR \> Encounter Management v1.2.0
  Completed

## <img src="images/icons/emoticons/72/1f31f.png"
class="emoticon emoticon-blue-star" data-emoji-id="1f31f"
data-emoji-shortname=":star2:" data-emoji-fallback="\uD83C\uDF1F"
data-emoticon-name="blue-star" width="16" height="16"
alt="(blue star)" /> Important topics

<table class="confluenceTable" data-layout="default"
data-local-id="7614e28e-ef23-4e90-98b4-a851af1b757f">
<tbody>
<tr class="header">
<th class="numberingColumn confluenceTh"></th>
<th class="confluenceTh"><p><strong>Topic</strong></p></th>
<th class="confluenceTh"><p><strong>Discussion</strong></p></th>
</tr>
&#10;<tr class="odd">
<td class="numberingColumn confluenceTd">1</td>
<td class="confluenceTd"><p>Clear solution requirements</p></td>
<td class="confluenceTd"><p><strong>1. Contract management</strong></p>
<ul>
<li><p><a href="https://gotecq-vn.atlassian.net/browse/TPF-1099"
class="jira-issue-key"><img
src="https://gotecq-vn.atlassian.net/rest/api/2/universal_avatar/view/type/issuetype/avatar/10310?size=medium"
class="icon" />TPF-1099</a> - Do not show dismissed documents in element
group preview when contract package status is submitted/ completed
Completed</p>
<ul>
<li><p>When package is <strong>approved</strong> by network, status of
dismissed element group is <strong>completed</strong>. Should we keep
this status as <strong>DISMISSED</strong> instead of
<strong>COMPLETED</strong> ?</p></li>
<li><p>Keep status is dismised</p></li>
</ul></li>
<li><p><a href="https://gotecq-vn.atlassian.net/browse/TPF-1032"
class="jira-issue-key"><img
src="https://gotecq-vn.atlassian.net/rest/api/2/universal_avatar/view/type/issuetype/avatar/10310?size=medium"
class="icon" />TPF-1032</a> - [C] Do not remove existing signatures if a
review is declined Completed</p>
<ul>
<li><p>When operator declines package, element group status is
<strong>DECLINED,</strong> status step is
<strong>ERROR</strong></p></li>
<li><p>How do we show the status/declined reason of network while
keeping provider’s sign request?</p></li>
<li><p>Should we keep status of element group as <strong>SIGNED</strong>
?</p></li>
<li><p>Vẫn giữ status SIGNED → change status của package thành
ubsubmitted</p></li>
</ul></li>
</ul>
<p><strong>2. Contact center</strong></p>
<ul>
<li><p>Update practitioner/organization/work item APIs</p></li>
<li><p><a
href="https://www.figma.com/file/pfjCJa7KGLTVd23OcBWxtP/GoTECQ-Contact-Center-rev3.0?node-id=5957%3A63821"
class="external-link" data-card-appearance="inline"
rel="nofollow">https://www.figma.com/file/pfjCJa7KGLTVd23OcBWxtP/GoTECQ-Contact-Center-rev3.0?node-id=5957%3A63821</a></p></li>
<li><p>Create view in contact</p></li>
</ul>
<p><strong>3. Claim management</strong></p>
<ul>
<li><p>Missing data, need data for data model</p></li>
<li><p><a
href="https://www.figma.com/file/HNhHz56t3ao9WKfI52H3q6/GoTECQ-Encounter-Management-rev2.2?node-id=9474%3A251819"
class="external-link" data-card-appearance="inline"
rel="nofollow">https://www.figma.com/file/HNhHz56t3ao9WKfI52H3q6/GoTECQ-Encounter-Management-rev2.2?node-id=9474%3A251819</a></p></li>
</ul>
<p><strong>4. PCP dashboard</strong></p>
<ul>
<li><p><a href="https://gotecq-vn.atlassian.net/browse/TPF-1924"
class="jira-issue-key"><img
src="https://gotecq-vn.atlassian.net/rest/api/2/universal_avatar/view/type/issuetype/avatar/10315?size=medium"
class="icon" />TPF-1924</a> - Comment on Dashboard Completed</p></li>
<li><p><a href="https://gotecq-vn.atlassian.net/browse/TPF-1928"
class="jira-issue-key"><img
src="https://gotecq-vn.atlassian.net/rest/api/2/universal_avatar/view/type/issuetype/avatar/10315?size=medium"
class="icon" />TPF-1928</a> - Starred dashboards Completed</p></li>
<li><p><a href="https://gotecq-vn.atlassian.net/browse/TPF-1926"
class="jira-issue-key"><img
src="https://gotecq-vn.atlassian.net/rest/api/2/universal_avatar/view/type/issuetype/avatar/10315?size=medium"
class="icon" />TPF-1926</a> - Show work-item list Completed</p></li>
<li><p>Create table to store dashboard, <strong>code-display, hoặc
dashboard có internal_id</strong></p></li>
</ul>
<p><strong>5. Member company</strong></p>
<ul>
<li><p>Member company type là Other</p></li>
<li><p>Auto generate company cho member -&gt;
create-standalone-user</p></li>
</ul>
<p><strong>6. Convert click here to button</strong></p>
<ul>
<li><p><a
href="https://gotecq-vn.atlassian.net/browse/TPF-2116?src=confmacro"
class="jira-issue-key">TPF-2116</a></p></li>
<li><p>Primary action</p></li>
</ul></td>
</tr>
<tr class="even">
<td class="numberingColumn confluenceTd">2</td>
<td class="confluenceTd"><p>Department issues</p></td>
<td class="confluenceTd"><p><strong>1. Dynamic
configuration</strong></p>
<ul>
<li><p>Back-End configuration</p></li>
<li><p>Reload config → Down server 1 khoảng thời gian</p></li>
<li><p>Signal protocol</p></li>
</ul></td>
</tr>
<tr class="odd">
<td class="numberingColumn confluenceTd">3</td>
<td class="confluenceTd"></td>
<td class="confluenceTd"></td>
</tr>
</tbody>
</table>

## <img src="images/icons/emoticons/72/2705.png"
class="emoticon emoticon-blue-star" data-emoji-id="2705"
data-emoji-shortname=":white_check_mark:" data-emoji-fallback="✅"
data-emoticon-name="blue-star" width="16" height="16"
alt="(blue star)" /> Action items

-  

Document generated by Confluence on May 04, 2023 09:15

[Atlassian](http://www.atlassian.com/)
